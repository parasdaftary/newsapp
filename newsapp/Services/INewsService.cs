﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Services
{
    public interface INewsService
    {
        Task<News> GetNews(string page);

        Task<Health> GetHealthNews(string page);

        Task<World> GetWorldNews(string page);

        Task<Technology> GetTechnologyNews(string page);

        Task<Sports> GetSportsNews(string page);

        Task<Entertainment> GetEntertainmentNews(string page);

        Task<Politics> GetPoliticsNews(string page);

        Task<Business> GetBusinessNews(string page);

        Task<Lifestyle> GetLifestyleNews(string page);

        string GetQueryString(string id);

        Task<NewsContent> GetNewsContentNews(string query);

        Task<NewsList> GetNewsList(string page, string category);

        Task<Video> GetVideo(string page);
    }
}