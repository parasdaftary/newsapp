﻿using JW;
using NewsFeed.Interfaces;

namespace NewsFeed.Services
{
    public class PagerViewModelService : IPagerViewModelService
    {
        private readonly IPagerViewModel _pagerViewModel;

        public PagerViewModelService(IPagerViewModel pagerViewModel)
        {
            _pagerViewModel = pagerViewModel;
        }

        public Pager GetPager(int? Page, int MaxPages, int? Size_Of_Page, int No_Of_Page,
                int TotalPages)
        {
            var pager = _pagerViewModel.ReturnPagerData(Page, MaxPages, Size_Of_Page, No_Of_Page, TotalPages);
            return pager;
        }
    }
}