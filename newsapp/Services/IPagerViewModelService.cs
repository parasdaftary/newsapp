﻿using JW;

namespace NewsFeed.Services
{
    public interface IPagerViewModelService
    {
        Pager GetPager(int? Page, int MaxPages, int? Size_Of_Page, int No_Of_Page,
                int TotalPages);
    }
}