﻿using NewsFeed.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsFeed.Services
{
    public interface INewsResultService
    {
        Task<List<NewsResult>> GetNewsResult(News news);

        Task<List<NewsResult>> GetHealthNewsResult(Health healthnews);

        Task<List<NewsResult>> GetWorldNewsResult(World worldnews);

        Task<List<NewsResult>> GetTechnologyNewsResult(Technology technologynews);

        Task<List<NewsResult>> GetSportsNewsResult(Sports sportsnews);

        Task<List<NewsResult>> GetEntertainmentNewsResult(Entertainment entertainmentnews);

        Task<List<NewsResult>> GetPoliticsNewsResult(Politics politicsnews);

        Task<List<NewsResult>> GetBusinessNewsResult(Business businessnews);

        Task<List<NewsResult>> GetLifestyleNewsResult(Lifestyle lifestylenews);

        NewsResult GetNewsContentNewsResult(NewsContent newsContent);

        Task<List<NewsResult>> GetNewsListNewsResult(NewsList newListnews);

        Task<List<NewsResult>> GetVideoNewsResult(Video videonews);
    }
}