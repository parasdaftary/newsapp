﻿using NewsFeed.Interfaces;
using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Services
{
    public class NewsService : INewsService
    {
        private readonly INews _news;
        private readonly IHealth _health;
        private readonly IWorld _world;
        private readonly ITechnology _technology;
        private readonly ISports _sports;
        private readonly IEntertainment _entertainment;
        private readonly IPolitics _politics;
        private readonly IBusiness _business;
        private readonly ILifestyle _lifestyle;
        private readonly INewsContent _newscontent;
        private readonly INewsList _newList;
        private readonly IVideo _video;

        public NewsService(INews news, IHealth health, IWorld world, ITechnology technology,
            ISports sports, IEntertainment entertainment, IPolitics politics, IBusiness business,
            ILifestyle lifestyle, INewsContent newsContent, INewsList newsList, IVideo video)
        {
            _news = news;
            _health = health;
            _world = world;
            _technology = technology;
            _sports = sports;
            _entertainment = entertainment;
            _politics = politics;
            _business = business;
            _lifestyle = lifestyle;
            _newscontent = newsContent;
            _newList = newsList;
            _video = video;
        }

       

        public async Task<News> GetNews(string page)
        {
            return await _news.ReturnNewsData(page);
        }

        public async Task<Health> GetHealthNews(string page)
        {
            return await _health.ReturnNewsData(page);
        }

        public async Task<World> GetWorldNews(string page)
        {
            return await _world.ReturnWorldNewsData(page);
        }

        public async Task<Technology> GetTechnologyNews(string page)
        {
            return await _technology.ReturnTechnologyNewsData(page);
        }

        public async Task<Sports> GetSportsNews(string page)
        {
            return await _sports.ReturnSportsNewsData(page);
        }

        public async Task<Entertainment> GetEntertainmentNews(string page)
        {
            return await _entertainment.ReturnEntertainmentData(page);
        }

        public async Task<Politics> GetPoliticsNews(string page)
        {
            return await _politics.ReturnPoliticsNewsData(page);
        }

        public async Task<Business> GetBusinessNews(string page)
        {
            return await _business.ReturnBusinessData(page);
        }

        public async Task<Lifestyle> GetLifestyleNews(string page)
        {
            return await _lifestyle.ReturnLifestyleData(page);
        }

        public string GetQueryString(string id)
        {
            return _newscontent.QueryBuilder(id);
        }

        public async Task<NewsContent> GetNewsContentNews(string query)
        {
            return await _newscontent.ReturnNewsContent(query);
        }

        public async Task<NewsList> GetNewsList(string page, string category)
        {
            return await _newList.ReturnNewsListData(page, category);
        }

        public async Task<Video> GetVideo(string page)
        {
            return await _video.ReturnVideoData(page);
        }
    }
}