﻿using NewsFeed.Interfaces;
using NewsFeed.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsFeed.Services
{
    public class NewsResultService : INewsResultService
    {
        private static INewsResult _newsResult;

        public NewsResultService(INewsResult newsResult)
        {
            _newsResult = newsResult;
        }

        public async Task<List<NewsResult>> GetNewsResult(News news)
        {
            return await _newsResult.ReturnNewsResultData(news);
        }

        public async Task<List<NewsResult>> GetHealthNewsResult(Health healthnews)
        {
            return await _newsResult.ReturnHealthNewsResultData(healthnews);
        }

        public async Task<List<NewsResult>> GetWorldNewsResult(World worldnews)
        {
            return await _newsResult.ReturnWorldNewsResultData(worldnews);
        }

        public async Task<List<NewsResult>> GetTechnologyNewsResult(Technology technologynews)
        {
            return await _newsResult.ReturnTechnologyNewsResultData(technologynews);
        }

        public async Task<List<NewsResult>> GetSportsNewsResult(Sports sportsnews)
        {
            return await _newsResult.ReturnSportsNewsResultData(sportsnews);
        }

        public async Task<List<NewsResult>> GetEntertainmentNewsResult(Entertainment entertainmentnews)
        {
            return await _newsResult.ReturnEntertainmentNewsResultData(entertainmentnews);
        }

        public async Task<List<NewsResult>> GetPoliticsNewsResult(Politics politicsnews)
        {
            return await _newsResult.ReturnPoliticsNewsResultData(politicsnews);
        }

        public async Task<List<NewsResult>> GetBusinessNewsResult(Business businessnews)
        {
            return await _newsResult.ReturnBusinessNewsResultData(businessnews);
        }

        public async Task<List<NewsResult>> GetLifestyleNewsResult(Lifestyle lifestylenews)
        {
            return await _newsResult.ReturnLifestyleNewsResultData(lifestylenews);
        }

        public NewsResult GetNewsContentNewsResult(NewsContent newsContent)
        {
            return _newsResult.ReturnNewsContentNewsResultData(newsContent);
        }

        public async Task<List<NewsResult>> GetNewsListNewsResult(NewsList newsListnews)
        {
            return await _newsResult.ReturnNewsListNewsResultData(newsListnews);
        }

        public async Task<List<NewsResult>> GetVideoNewsResult(Video videonews)
        {
            return await _newsResult.ReturnVideoNewsResultData(videonews);
        }
    }
}