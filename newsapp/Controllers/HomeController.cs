﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using NewsFeed.Interfaces;
using NewsFeed.Models;
using NewsFeed.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NewsFeed.Controllers
{
    public class HomeController : Microsoft.AspNetCore.Mvc.Controller
    {
        // private readonly ILogger<HomeController> _logger;
        private readonly INewsService _newsService;

        private readonly INewsResultService _newsResultService;
        private readonly IPagerViewModelService _pagerViewModelService;
        private readonly IPagerViewModel _pagerViewModel;
        private readonly IMemoryCache _memoryCache;

        public HomeController(ILogger<HomeController> logger, INewsService newsService, INewsResultService newsResultService,
            IPagerViewModelService pagerViewModelService, IPagerViewModel pagerViewModel, IMemoryCache memoryCache)
        {
            // _logger = logger;
            _newsService = newsService;
            _newsResultService = newsResultService;
            _pagerViewModelService = pagerViewModelService;
            _pagerViewModel = pagerViewModel;
            _memoryCache = memoryCache;
        }

        public async Task<IActionResult> Index(int? page = 1)
        {
            int? apiPage = page;

            if (!_memoryCache.TryGetValue("News", out List<NewsResult> newsResults))
            {
                var newsData = await _newsService.GetNews(apiPage.ToString());
                _memoryCache.Set("News", await _newsResultService.GetNewsResult(newsData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            newsResults = _memoryCache.Get("News") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("HealthNews", out List<NewsResult> healthResult))
            {
                var healthData = await _newsService.GetHealthNews(apiPage.ToString());
                _memoryCache.Set("HealthNews", await _newsResultService.GetHealthNewsResult(healthData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            healthResult = _memoryCache.Get("HealthNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("WorldhNews", out List<NewsResult> worldResult))
            {
                var worldData = await _newsService.GetWorldNews(apiPage.ToString());
                _memoryCache.Set("WorldhNews", await _newsResultService.GetWorldNewsResult(worldData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            worldResult = _memoryCache.Get("WorldhNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("TechNews", out List<NewsResult> techResult))
            {
                var techData = await _newsService.GetTechnologyNews(apiPage.ToString());
                _memoryCache.Set("TechNews", await _newsResultService.GetTechnologyNewsResult(techData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            techResult = _memoryCache.Get("TechNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("SportNews", out List<NewsResult> sportResult))
            {
                var techData = await _newsService.GetSportsNews(apiPage.ToString());
                _memoryCache.Set("SportNews", await _newsResultService.GetSportsNewsResult(techData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            sportResult = _memoryCache.Get("SportNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("EntertainmentNews", out List<NewsResult> entertainmentResult))
            {
                var entertainmentData = await _newsService.GetEntertainmentNews(apiPage.ToString());
                _memoryCache.Set("EntertainmentNews", await _newsResultService.GetEntertainmentNewsResult(entertainmentData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            entertainmentResult = _memoryCache.Get("EntertainmentNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("PoliticsNews", out List<NewsResult> politicsResult))
            {
                var politicsData = await _newsService.GetPoliticsNews(apiPage.ToString());
                _memoryCache.Set("PoliticsNews", await _newsResultService.GetPoliticsNewsResult(politicsData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            politicsResult = _memoryCache.Get("PoliticsNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("BusinessNews", out List<NewsResult> businessResult))
            {
                var businessData = await _newsService.GetBusinessNews(apiPage.ToString());
                _memoryCache.Set("BusinessNews", await _newsResultService.GetBusinessNewsResult(businessData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            businessResult = _memoryCache.Get("BusinessNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("LifeNews", out List<NewsResult> lifestyleResult))
            {
                var lifestyleData = await _newsService.GetLifestyleNews(apiPage.ToString());
                _memoryCache.Set("LifeNews", await _newsResultService.GetLifestyleNewsResult(lifestyleData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            lifestyleResult = _memoryCache.Get("LifeNews") as List<NewsResult>;

            if (!_memoryCache.TryGetValue("VideoNews", out List<NewsResult> videoResult))
            {
                var videoData = await _newsService.GetVideo(apiPage.ToString());
                _memoryCache.Set("VideoNews", await _newsResultService.GetVideoNewsResult(videoData), _pagerViewModel.cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(300));
            }
            videoResult = _memoryCache.Get("VideoNews") as List<NewsResult>;

            var view = new NewsResult
            {
                //Pager = pager,
                news = newsResults,
                healthnews = healthResult,
                worldnews = worldResult,
                technologynews = techResult,
                sportsnews = sportResult,
                entertainmentnews = entertainmentResult,
                politicsnews = politicsResult,
                businessnews = businessResult,
                lifestylenews = lifestyleResult,
                videonews = videoResult
            };

            return View(view);
        }

        public IActionResult View()
        {
            //string search = HttpContext.Request.Query["q"];
            return View("View");
        }

        public IActionResult Privacy()
        {
            return base.View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void GetJsonData(int? page)
        {
            throw new NotImplementedException();
        }
    }
}