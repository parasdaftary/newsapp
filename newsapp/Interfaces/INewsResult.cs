﻿using JW;
using NewsFeed.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface INewsResult
    {
        string apiUrl { get; set; }
        Fields fields { get; set; }
        string id { get; set; }
        bool isHosted { get; set; }
        Pager Pager { get; set; }
        string pillarId { get; set; }
        string pillarName { get; set; }
        string sectionId { get; set; }
        string sectionName { get; set; }
        string type { get; set; }
        DateTime webPublicationDate { get; set; }
        string webTitle { get; set; }
        string webUrl { get; set; }

        List<NewsResult> news { get; set; }
        List<NewsResult> healthnews { get; set; }
        List<NewsResult> worldnews { get; set; }
        List<NewsResult> technologynews { get; set; }
        List<NewsResult> sportsnews { get; set; }
        List<NewsResult> entertainmentnews { get; set; }
        List<NewsResult> politicsnews { get; set; }
        List<NewsResult> businessnews { get; set; }
        List<NewsResult> lifestylenews { get; set; }
        List<NewsResult> videonews { get; set; }

        List<NewsResult> newsResults { get; set; }
        List<NewsResult> healthResult { get; set; }
        List<NewsResult> worldResult { get; set; }
        List<NewsResult> techResult { get; set; }
        List<NewsResult> sportResult { get; set; }
        List<NewsResult> entertainmentResult { get; set; }
        List<NewsResult> politicsResult { get; set; }
        List<NewsResult> businessResult { get; set; }
        List<NewsResult> lifestyleResult { get; set; }
        List<NewsResult> videoResult { get; set; }
        NewsResult newscontent { get; set; }
        List<string> sectionFilter { get; set; }
        List<string> ContentId { get; set; }

        List<string> getSectionFilter(List<NewsResult> newsResult);

        Task<List<NewsResult>> ReturnNewsResultData(News news);

        Task<List<NewsResult>> ReturnHealthNewsResultData(Health healthnews);

        Task<List<NewsResult>> ReturnWorldNewsResultData(World worldnews);

        Task<List<NewsResult>> ReturnTechnologyNewsResultData(Technology technologynews);

        Task<List<NewsResult>> ReturnSportsNewsResultData(Sports sportsnews);

        Task<List<NewsResult>> ReturnEntertainmentNewsResultData(Entertainment entertainmentnews);

        Task<List<NewsResult>> ReturnPoliticsNewsResultData(Politics politicsnews);

        Task<List<NewsResult>> ReturnBusinessNewsResultData(Business businessnews);

        Task<List<NewsResult>> ReturnLifestyleNewsResultData(Lifestyle lifestylenews);

        NewsResult ReturnNewsContentNewsResultData(NewsContent newsContent);

        Task<List<NewsResult>> ReturnNewsListNewsResultData(NewsList newsList);

        Task<List<NewsResult>> ReturnVideoNewsResultData(Video videonews);
    }
}