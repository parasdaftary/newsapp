﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface INewsContent
    {
        NewsResponse response { get; set; }
        NewsContent newsContent { get; set; }

        string QueryBuilder(string id);

        Task<NewsContent> ReturnNewsContent(string query);
    }
}