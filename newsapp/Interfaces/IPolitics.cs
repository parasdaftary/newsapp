﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface IPolitics
    {
        NewsResponse response { get; set; }

        //string GetJsonData(int? page);
        Politics politics { get; set; }
        Task<Politics> ReturnPoliticsNewsData(string searchCritera);
    }
}