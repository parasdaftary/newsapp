﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface IBusiness
    {
        NewsResponse response { get; set; }

        //string GetJsonData(int? page);
        Business business { get; set; }
        Task<Business> ReturnBusinessData(string searchCritera);
    }
}