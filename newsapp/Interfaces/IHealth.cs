﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface IHealth
    {
        NewsResponse response { get; set; }
        Health health { get; set; }

        //string GetJsonData(int? page);

        Task<Health> ReturnNewsData(string searchCritera);
    }
}