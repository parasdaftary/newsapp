﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface ITechnology
    {
        NewsResponse response { get; set; }

        //string GetJsonData(int? page);
        Technology technology { get; set; }

        Task<Technology> ReturnTechnologyNewsData(string searchCritera);
    }
}