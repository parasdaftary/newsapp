﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface IEntertainment
    {
        NewsResponse response { get; set; }

        //string GetJsonData(int? page);
        Entertainment entertainment { get; set; }
        Task<Entertainment> ReturnEntertainmentData(string searchCritera);
    }
}