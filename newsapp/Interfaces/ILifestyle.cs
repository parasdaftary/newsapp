﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface ILifestyle
    {
        NewsResponse response { get; set; }
        Lifestyle lifestyle { get; set; }

        //string GetJsonData(int? page);

        Task<Lifestyle> ReturnLifestyleData(string searchCritera);
    }
}