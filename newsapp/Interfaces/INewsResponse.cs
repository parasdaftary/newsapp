﻿using NewsFeed.Models;
using System.Collections.Generic;

namespace NewsFeed.Interfaces
{
    public interface INewsResponse
    {
        int currentPage { get; set; }
        string orderBy { get; set; }
        int pages { get; set; }
        int pageSize { get; set; }
        List<NewsResult> results { get; set; }
        int startIndex { get; set; }
        string status { get; set; }
        int total { get; set; }
        string userTier { get; set; }
        NewsResult content { get; set; }
    }
}