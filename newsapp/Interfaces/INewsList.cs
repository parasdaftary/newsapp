﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface INewsList
    {
        NewsResponse response { get; set; }
        NewsList newsList { get; set; }

        //string GetJsonData(int? page);

        Task<NewsList> ReturnNewsListData(string searchCritera, string category);
    }
}