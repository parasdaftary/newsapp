﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface INews
    {
        NewsResponse response { get; set; }
        News news { get; set; }

        //string GetJsonData(int? page);

        Task<News> ReturnNewsData(string searchCritera);
    }
}