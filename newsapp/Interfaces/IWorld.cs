﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface IWorld
    {
        NewsResponse response { get; set; }

        //string GetJsonData(int? page);
        World world { get; set; }
        Task<World> ReturnWorldNewsData(string searchCritera);
    }
}