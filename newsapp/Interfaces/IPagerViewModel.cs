﻿using JW;
using System.Runtime.Caching;

namespace NewsFeed.Interfaces
{
    public interface IPagerViewModel
    {
        int MaxPages { get; set; }
        int? Size_Of_Page { get; set; }
        int No_Of_Page { get; set; }
        int? Page { get; set; }
        int TotalPages { get; set; }

        Pager ReturnPagerData(int? Page, int MaxPages, int? Size_Of_Page, int No_Of_Page, int TotalPages);

        CacheItemPolicy cacheItemPolicy { get; set; }
    }
}