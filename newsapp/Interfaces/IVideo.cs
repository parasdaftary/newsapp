﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface IVideo
    {
        NewsResponse response { get; set; }
        Video video { get; set; }

        //string GetJsonData(int? page);

        Task<Video> ReturnVideoData(string searchCritera);
    }
}