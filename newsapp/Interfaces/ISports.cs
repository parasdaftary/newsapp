﻿using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeed.Interfaces
{
    public interface ISports
    {
        NewsResponse response { get; set; }

        //string GetJsonData(int? page);
        Sports sports { get; set; }
        Task<Sports> ReturnSportsNewsData(string searchCritera);
    }
}