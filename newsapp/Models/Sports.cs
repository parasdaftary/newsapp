﻿using NewsFeed.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsFeed.Models
{
    public class Sports : ISports
    {
        public NewsResponse response { get; set; }
        public Sports sports { get => new Sports(); set =>  new Sports(); }

        public async Task<Sports> ReturnSportsNewsData(string page)
        {
            using (var client = new HttpClient())
            {
                var url = new Uri("https://content.guardianapis.com/au/sport?show-fields=thumbnail&page=" + page + "&api-key=dca50a7c-dad5-4595-b070-0b238af6652c&format=json");

                var response = await client.GetAsync(url);

                string json;

                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }

                return JsonConvert.DeserializeObject<Sports>(json);
            }
        }
    }
}