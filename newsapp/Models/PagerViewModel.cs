﻿using JW;
using NewsFeed.Interfaces;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace NewsFeed.Models
{
    public class PagerViewModel : IPagerViewModel
    {
        public int MaxPages { get; set; }
        public int? Size_Of_Page { get; set; }
        public int No_Of_Page { get; set; }
        public int? Page { get; set; }
        public int TotalPages { get; set; }

        public List<string> ContentId { get; set; }
        public CacheItemPolicy cacheItemPolicy { get => new CacheItemPolicy(); set => new CacheItemPolicy(); }

        public Pager ReturnPagerData(int? Page, int MaxPages, int? Size_Of_Page, int No_Of_Page, int TotalPages)
        {
            MaxPages = 10;
            int pageSize = (Size_Of_Page ?? 10);
            No_Of_Page = (Page ?? 1);
            Pager pager = new Pager(20000, No_Of_Page, pageSize, MaxPages);

            return pager;
        }
    }
}