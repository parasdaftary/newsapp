﻿using NewsFeed.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsFeed.Models
{
    public class World : IWorld
    {
        public NewsResponse response { get; set; }
        public World world { get =>  new World(); set =>  new World(); }

        public async Task<World> ReturnWorldNewsData(string page)
        {
            using (var client = new HttpClient())
            {
                var url = new Uri("https://content.guardianapis.com/world?show-fields=all&page=" + page + "&api-key=dca50a7c-dad5-4595-b070-0b238af6652c&format=json");

                var response = await client.GetAsync(url);

                string json;

                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }

                return JsonConvert.DeserializeObject<World>(json);
            }
        }
    }
}