﻿using NewsFeed.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsFeed.Models
{
    public class Entertainment : IEntertainment
    {
        public NewsResponse response { get; set; }
        public Entertainment entertainment { get =>  new Entertainment(); set =>  new Entertainment(); }

        public async Task<Entertainment> ReturnEntertainmentData(string page)
        {
            using (var client = new HttpClient())
            {
                var url = new Uri("https://content.guardianapis.com/au/culture?show-fields=thumbnail&page=" + page + "&api-key=dca50a7c-dad5-4595-b070-0b238af6652c&format=json");

                var response = await client.GetAsync(url);

                string json;

                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }
                return JsonConvert.DeserializeObject<Entertainment>(json);
            }
        }
    }
}