﻿using NewsFeed.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsFeed.Models
{
    public class Health : IHealth
    {
        public NewsResponse response { get; set; }
        public Health health { get => new Health(); set =>  new Health(); }

        public async Task<Health> ReturnNewsData(string page)
        {
            using (var client = new HttpClient())
            {
                var url = new Uri("https://content.guardianapis.com/lifeandstyle/health-and-wellbeing?show-fields=thumbnail&page=" + page + "&api-key=dca50a7c-dad5-4595-b070-0b238af6652c&format=json");

                var response = await client.GetAsync(url);

                string json;

                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }

                return JsonConvert.DeserializeObject<Health>(json);
            }
        }
    }
}