﻿using NewsFeed.Interfaces;
using System.Collections.Generic;

namespace NewsFeed.Models
{
    public class NewsResponse : INewsResponse
    {
        public string status { get; set; }
        public string userTier { get; set; }
        public int total { get; set; }
        public int startIndex { get; set; }
        public int pageSize { get; set; }
        public int currentPage { get; set; }
        public int pages { get; set; }
        public string orderBy { get; set; }
        public List<NewsResult> results { get; set; }
        public NewsResult content { get; set; }
    }
}