﻿using NewsFeed.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsFeed.Models
{
    public class Technology : ITechnology
    {
        public NewsResponse response { get; set; }
        public Technology technology { get =>  new Technology(); set => new Technology(); }

        public async Task<Technology> ReturnTechnologyNewsData(string page)
        {
            using (var client = new HttpClient())
            {
                var url = new Uri("https://content.guardianapis.com/au/technology?show-fields=all&show-elements=all&page=" + page + "&api-key=dca50a7c-dad5-4595-b070-0b238af6652c&format=json");

                var response = await client.GetAsync(url);

                string json;

                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }

                return JsonConvert.DeserializeObject<Technology>(json);
            }
        }
    }
}