﻿using NewsFeed.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsFeed.Models
{
    public class NewsContent : INewsContent
    {
        public NewsResponse response { get; set; }
        public NewsContent newsContent { get => new NewsContent(); set =>  new NewsContent(); }

        public string QueryBuilder(string id)
        {
            //var queryString = new List<string>();
            //queryString.Add(sectionid.ToLower());
            //queryString.Add(date.Replace('-','/'));
            //queryString.Add(id.ToLower());
            return id.Replace('_', '/');
        }

        public async Task<NewsContent> ReturnNewsContent(string query)
        {
            using (var client = new HttpClient())
            {
                var url = new Uri("https://content.guardianapis.com/" + query + "?api-key=dca50a7c-dad5-4595-b070-0b238af6652c&show-fields=thumbnail,byline,body,bodyText,main&format=json");
                //var url = new Uri("https://content.guardianapis.com/lifeandstyle/health-and-wellbeing?show-fields=thumbnail&page=1&api-key=dca50a7c-dad5-4595-b070-0b238af6652c&format=json");

                var response = await client.GetAsync(url);

                string json;

                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }
                return JsonConvert.DeserializeObject<NewsContent>(json);
            }
        }
    }
}