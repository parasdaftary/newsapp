﻿using NewsFeed.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsFeed.Models
{
    public class NewsList : INewsList
    {
        public NewsResponse response { get; set; }
        public NewsList newsList { get =>  new NewsList(); set =>  new NewsList(); }

        public async Task<NewsList> ReturnNewsListData(string page, string category)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var url = new Uri("https://content.guardianapis.com/" + category + "?show-fields=all&page-size=16&show-blocks=all&page=" + page + "&api-key=dca50a7c-dad5-4595-b070-0b238af6652c&format=json");

                    var response = await client.GetAsync(url);

                    string json;

                    using (var content = response.Content)
                    {
                        json = await content.ReadAsStringAsync();
                    }
                    return JsonConvert.DeserializeObject<NewsList>(json);
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}