﻿using JW;
using NewsFeed.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace NewsFeed.Models
{
    public class Fields
    {
        public string thumbnail { get; set; }
        public string byline { get; set; }
        public string body { get; set; }
        public string bodyText { get; set; }
        public string main { get; set; }
        public string standfirst { get; set; }
    }

    public class NewsResult : INewsResult
    {
        public string id { get; set; }
        public string type { get; set; }
        public string sectionId { get; set; }
        public string sectionName { get; set; }
        public DateTime webPublicationDate { get; set; }
        public string webTitle { get; set; }
        public string webUrl { get; set; }
        public string apiUrl { get; set; }
        public Fields fields { get; set; }
        public bool isHosted { get; set; }
        public string pillarId { get; set; }
        public string pillarName { get; set; }

        public Pager Pager { get; set; }
        public List<NewsResult> news { get; set; }
        public List<NewsResult> healthnews { get; set; }
        public List<NewsResult> worldnews { get; set; }
        public List<NewsResult> technologynews { get; set; }
        public List<NewsResult> sportsnews { get; set; }
        public List<NewsResult> entertainmentnews { get; set; }
        public List<NewsResult> politicsnews { get; set; }
        public List<NewsResult> businessnews { get; set; }
        public List<NewsResult> lifestylenews { get; set; }
        public List<NewsResult> videonews { get; set; }

        public List<NewsResult> newsResults { get; set; }
        public List<NewsResult> healthResult { get; set; }
        public List<NewsResult> worldResult { get; set; }
        public List<NewsResult> techResult { get; set; }
        public List<NewsResult> sportResult { get; set; }
        public List<NewsResult> entertainmentResult { get; set; }
        public List<NewsResult> politicsResult { get; set; }
        public List<NewsResult> businessResult { get; set; }
        public List<NewsResult> lifestyleResult { get; set; }
        public List<NewsResult> videoResult { get; set; }

        public List<string> sectionFilter { get; set; }
        public List<string> ContentId { get; set; }
        public string apiId { get; set; }
        public NewsResult newscontent { get; set; }

        public List<string> getSectionFilter(List<NewsResult> newsResults)
        {
            var groupSection = newsResults.GroupBy(x => x.sectionName).Select(x => x);
            var filter = new List<string>();

            foreach (var section in groupSection)
            {
                filter.Add(section.Key.ToString() + " " + section.Count().ToString());
            }
            return filter;
        }

        public async Task<List<NewsResult>> ReturnNewsResultData(News news)
        {
            var result = await (news.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnHealthNewsResultData(Health healthnews)
        {
            var result = await (healthnews.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnWorldNewsResultData(World worldnews)
        {
            var result = await (worldnews.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnTechnologyNewsResultData(Technology technologynews)
        {
            var result = await (technologynews.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnSportsNewsResultData(Sports sportsnews)
        {
            var result = await (sportsnews.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnEntertainmentNewsResultData(Entertainment entertainmentnews)
        {
            var result = await (entertainmentnews.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnPoliticsNewsResultData(Politics politicsnews)
        {
            var result = await (politicsnews.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnBusinessNewsResultData(Business businessnews)
        {
            var result = await (businessnews.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnLifestyleNewsResultData(Lifestyle lifestylenews)
        {
            var result = await (lifestylenews.response.results).ToListAsync();
            return result;
        }

        public NewsResult ReturnNewsContentNewsResultData(NewsContent newsContent)
        {
            var result = newsContent.response.content;
            return result;
        }

        public async Task<List<NewsResult>> ReturnNewsListNewsResultData(NewsList newsList)
        {
            var result = await (newsList.response.results).ToListAsync();
            return result;
        }

        public async Task<List<NewsResult>> ReturnVideoNewsResultData(Video videonews)
        {
            var result = await (videonews.response.results).ToListAsync();
            return result;
        }
    }
}