﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace NewsFeed.UrlHelpers
{
    public static class UrlExtensions
    {
        /// <summary>
        /// Generates a fully qualified URL to the specified content by using the specified content path.
        /// </summary>
        /// <param name="url">The URL helper.</param>
        /// <param name="contentPath">The content path.</param>
        /// <returns>The absolute URL.</returns>

        //static readonly IPagerViewModel _pagerViewModel;
        //static UrlExtensions()
        //{
        //    IPagerViewModel pager = new PagerViewModel();
        //    _pagerViewModel = pager;
        //}

        public static string AbsoluteContent(this IUrlHelper url, string contentPath)
        {
            var request = url.ActionContext.HttpContext.Request;
            return new Uri(new Uri(request.Scheme + "://" + request.Host.Value), url.Content(contentPath)).ToString();
        }

        public static string GetContentUrl(this IUrlHelper url, string contentPath)
        {
            string id = contentPath.Replace('/', '_');

            // string decodeContent = WebUtility.HtmlDecode(contentPath);
            return id;
        }

        public static string GetContentDate(this IUrlHelper url, DateTime contentPath)
        {
            DateTime contentDate = contentPath;
            string cDate = contentDate.ToString("yyyy-MMM-dd");

            // string decodeContent = WebUtility.HtmlDecode(contentPath);
            return cDate.ToLower();
        }

        public static string GetSecureUrl(this IUrlHelper url, string contentPath)
        {
            string sURL = contentPath.Replace("http", "https");
            sURL = sURL.Replace("httpss", "https");

            // string decodeContent = WebUtility.HtmlDecode(contentPath);
            return sURL;
        }
    }
}