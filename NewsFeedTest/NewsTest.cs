using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewsFeed.Interfaces;
using NewsFeed.Models;
using System.Threading.Tasks;

namespace NewsFeedTest
{
    [TestClass]
    public class NewsTest
    {
        private readonly INews _news;
        private readonly IHealth _health;

        public NewsTest()
        {
            var services = new ServiceCollection();
            services.AddSingleton<INews, News>();
            services.AddSingleton<IHealth, Health>();
            var serviceProvider = services.BuildServiceProvider();
            _news = serviceProvider.GetRequiredService<INews>();
            _health = serviceProvider.GetRequiredService<IHealth>();
        }

        [TestMethod]
        public async Task CheckJsonResponse()
        {
            //Arrange
            string apiPage = "1";
            string status = "ok";

            //Act
            var response = await _news.ReturnNewsData(apiPage);

            //Assert
            Assert.AreEqual(response.response.status, status);
        }
        [TestMethod]
        public async Task CheckHealthJsonResponse()
        {
            //Arrange
            string apiPage = "1";
            string status = "ok";

            //Act
            var response = await _health.ReturnNewsData(apiPage);

            //Assert
            Assert.AreEqual(response.response.status, status);
        }
    }
}